use StatError;
use num::Num;
use num::cast::ToPrimitive;
use Staticized;
use StaticizedUtil;



pub struct RVec<S> {
    data: Vec<S>
}

/// Create A new RVec
///
/// ```
/// extern crate r_stat;
/// use r_stat::RVec;
/// fn main() {
///   let pop_data = RVec::new(vec!(1.0,1.54, 1.34));
/// }
/// ```
impl <S> RVec<S> {
    pub fn new(data_set: Vec<S>) -> Self {
        RVec {
            data:  data_set
        }
    }
}

impl <S: Num + ToPrimitive> Staticized for RVec<S> {
    type Data = S;
    fn mean(&self) -> Result<f64, StatError> {
        let temp: &[S] = self.data.as_ref();
        let temp: Vec<f64> = temp.iter().map(|x| x.to_f64().unwrap()).collect();
        let total = temp.iter().fold(0.0,|total, x| total + *x);
        let n = temp.iter().count() as f64;
        let result = total/n;
        match result.is_normal() {
            true => Ok(result),
            false => Err(StatError::new("Failed to get the variance for data set."))
        }
    }

    fn variance(&self) -> Result<f64, StatError> {
        let temp: &[S] = self.data.as_ref();
        let temp: Vec<f64> = temp.iter().map(|x| x.to_f64().unwrap()).collect();
        let mean: f64 = self.mean().unwrap();
        let size = temp.iter().count() as f64;
        let variance: f64 = temp.iter().fold(0.0,
            |variance, x|
                variance + (*x as f64 - mean).powi(2))/size;
        match variance.is_normal() {
            true => Ok(variance),
            false => Err(StatError::new("Failed to get the variance for data set."))
        }
    }

    fn standard_div(&self)->Result<f64, StatError> {
        if let Ok(variance) = self.variance() {
            let mut sd: f64 = variance;
            sd = sd.sqrt();
            println!("{:?}", sd);
            match sd.is_normal() {
                true => Ok(sd),
                false => Err(StatError::new("Failed to get the Standard Divation for data set."))
            }
        }
        else {
            Err(StatError::new("Failed to get the Standard Divation for data set."))
        }
    }
}

impl <S: Num + ToPrimitive> StaticizedUtil for RVec<S> {
    type Data = S;
    fn frequency(&self, search_value: S) -> usize {
        let temp: &[S] = self.data.as_ref();
        temp.iter().filter(|x| **x == search_value).count()
    }
}
