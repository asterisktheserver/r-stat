use Staticized;
use std::f64::consts::{E, PI};


pub struct Guassian <T> {
    population: T
}

impl <T> Guassian<T>
where T: Staticized,
 {
    ///  Determine the mean of the data set.
    ///
    /// ```rust
    /// extern crate r_stat;
    /// use r_stat::RVec;
    /// use r_stat::Staticized;
    /// use r_stat::Guassian;
    ///
    /// fn main() {
    ///     let data_set = RVec::new(vec!(60,50,10));
    ///     let gauss = Guassian::new(data_set);
    ///     assert_eq!(gauss.density_norm(10.0), 0.0003259262352547678);
    /// }
    /// ```
    pub fn density_norm(&self, x: f64)-> f64 {
        let variance : f64 = self.population.variance().unwrap();
        let mean : f64 = self.population.mean().unwrap();
         1.0/(variance * (2.0 * PI).sqrt()) *  E.powf(-(x - mean).powf(2.0)/(2.0*variance))
    }

    pub fn new(pop: T) -> Self {
        Guassian {
            population: pop
        }
    }
}
