use std::error::Error;
use std::fmt;
#[derive(Debug, PartialEq)]
pub struct StatError {
    description: String
}
impl Error for StatError {
    fn description(&self) -> &str {
        return self.description.as_str();
    }
}

impl fmt::Display for StatError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description)
    }
}

impl StatError {
    pub fn new(description: &str) -> Self {
        StatError {
            description: String::from(description)
        }
    }
}
