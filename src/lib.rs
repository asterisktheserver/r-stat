pub mod rstructures;
pub mod distributions;
pub mod traits;
pub use rstructures::rvec::RVec;
pub mod errors;
pub use errors::stat_error::StatError;
pub use traits::stats::{Staticized, StaticizedUtil};
pub use distributions::gaussian::Guassian;
extern crate num;
