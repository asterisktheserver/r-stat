use StatError;
/// Basic Stat Computations. That do not fit with Staticized.
pub trait StaticizedUtil {
    type Data;
    /// Determine the frequency of a specific value.
    ///
    /// ```rust
    /// extern crate r_stat;
    /// use r_stat::{RVec, StaticizedUtil};
    ///
    /// fn main() {
    ///     let data_set = RVec::<i16>::new(vec!(1,2,3,3,2,1));
    ///     let freq: usize = data_set.frequency(3);
    ///     assert_eq!(freq, 2);
    /// }
    /// ```
    fn frequency(&self, search_value: Self::Data) -> usize;
}

/// Basic Stat Computations.
pub trait Staticized {
    type Data;
    ///  Determine the mean of the data set.
    ///
    /// ```rust
    /// extern crate r_stat;
    /// use r_stat::{RVec,Staticized};
    ///
    /// fn main() {
    ///     let data_set = RVec::new(vec!(60,50,10));
    ///     let mean: f64 = 40.0;
    ///     assert_eq!(data_set.mean(), Ok(mean));
    /// }
    /// ```
    fn mean(&self) -> Result<f64, StatError>;
    ///  Determine the Variance of the data set.quantiles
    ///
    /// ```rust
    /// extern crate r_stat;
    /// use r_stat::{RVec, Staticized};
    /// fn main() {
    ///     let data_set = RVec::new(vec!(60,50,10));
    ///     let hand_calc_variance: f64 = 466.6666666666667;
    ///     let variance: f64 = data_set.variance().unwrap();
    ///     assert_eq!(hand_calc_variance, variance);
    /// }
    /// ```
    fn variance(&self) -> Result<f64, StatError>;
    ///  Determine the Standard Divation of the data set.
    ///
    /// ```rust
    /// extern crate r_stat;
    /// use r_stat::{RVec, Staticized};
    ///
    /// fn main() {
    ///     let data_set = RVec::new(vec!(60,50,10));
    ///     let hand_calc_sd : f64 = 21.602468994692867;
    ///     let sd: f64 = data_set.standard_div().unwrap();
    ///     assert_eq!(sd, hand_calc_sd);
    /// }
    /// ```
    fn standard_div(&self) -> Result <f64, StatError>;

}
